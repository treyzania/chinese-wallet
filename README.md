# Chinese Wallet

Script to derive a wallet seed from numbers on fortune cookies.  Proof of concept.

It takes the lucky numbers as command line arguments, fails if you give less then 36.  Override this with `OKAY=1`.

Requires [python-bitcoinlib](https://github.com/petertodd/python-bitcoinlib).

## Disclaimer(s)

* Don't use this with real money.  It should take the numbers from stdin since that's more secure.

* I'm well aware that fortune cookies aren't really a Chinese invention.
