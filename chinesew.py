#!/usr/bin/env python3

import os
import sys
import hashlib

import bitcoin.core.key as btckey

# Check that we don't do dumb stuff.
keynums = sys.argv[1:]
if len(keynums) < 36 and not 'OKAY' in os.environ:
	print('you should have more numbers, min 36')
	sys.exit(1)

# Concat them args as bytes.
buf = bytes([])
for n in keynums:
	buf += bytes([int(n)])

print('Bytes:\n\t' + buf.hex())

# Hash it.
m = hashlib.sha256()
m.update(buf)
hash = m.digest()

print('Hash:\n\t' + hash.hex())

# Now make a key from the hash.
k = btckey.CECKey()
k.set_secretbytes(hash)

print('Privkey:\n\t' + k.get_privkey().hex())
